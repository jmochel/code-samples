package org.saltations.samples.traits;

import org.apache.commons.beanutils.DynaBean;

import java.time.Duration;
import java.time.LocalDateTime;


public interface LocatableInTime extends DynaBean
{
    String START_TIME_PROP = "INTIME_START_TIME";
    String DURATION_PROP = "INTIME_DURATION";
    String INUSE_PROP = "INTIME_INUSE";

    default LocalDateTime getStartTime()
    {
        return (LocalDateTime) get(START_TIME_PROP);
    }

    default Duration getDuration()
    {
        return (Duration) get(DURATION_PROP);
    }

    default Boolean existsInTime()
    {
        return (Boolean) get(INUSE_PROP);
    }

}
