package org.saltations.samples.selftypes;

import lombok.NonNull;

import java.util.Locale;
import java.util.function.Predicate;

import static java.lang.String.format;

/**
 * Base class for all checks.
 *
 * @param <S> the "self" type of this Check class. Please read "Emulating  'self types' using Java Generics to simplify fluent API implementation"
 *
 * @param <A> the type of the "actual" value.
 *
 */

public abstract class AbstractCheck<S extends AbstractCheck<S, A>, A> implements Check<S,A>
{
    protected final A actual;
    protected final S myself;
    protected final CheckInfo info;

    /**
     * Primary constructor
     *
     * @param selfType
     * @param actual
     */

    protected AbstractCheck(Class<?> selfType, A actual)
    {
        this.myself = (S) selfType.cast(this);
        this.actual = actual;
        this.info = new CheckInfo();
    }

    /**
     * Checks the condition of the against and issues an exception with the formatted message
     *
     * @param predicate The against
     *
     * @throws IllegalArgumentException if {@code against} is false
     */

    @Override
    public S against(Predicate<A> predicate)
    {
        if (!predicate.test(actual))
        {
            throw new IllegalArgumentException(info.getMsg());
        }

        return myself;
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public S as(@NonNull String description, @NonNull Object... args)
    {
        info.setMsg(format(description, args));

        return myself;
    }



}
