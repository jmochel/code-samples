package org.saltations.samples.selftypes;

import lombok.NonNull;

/**
 * Created by jmochel on 5/3/16.
 */
public interface Describeable<S extends Describeable<S>>
{
    /**
     * Description of this object using {@link String#format(String, Object...)}.
     *
     * @param description the new description.
     * @param args optional parameter if description is a template with subsititutable strings.
     *
     * @return {@code this}.
     */

    S as(@NonNull String description, Object... args);

}
