package org.saltations.samples.selftypes;

import lombok.Data;

/**
 * Created by jmochel on 5/3/16.
 */

@Data
public class CheckInfo
{
    private String msg = "";
}
