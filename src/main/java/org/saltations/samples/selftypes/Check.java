package org.saltations.samples.selftypes;


import java.util.function.Predicate;

/**
 * Contract for all Check objects.
 *
 * @param <S> the "self" type of this Check class. Please read "Emulating  'self types' using Java Generics to simplify fluent API implementation"
 * @param <A> the type of the "actual" value.
 *
 * @author Jim Mochel
 */

public interface Check<S extends Check<S, A>, A> extends Describeable<S>
{
    /**
     * Tests the content against the predicate.
     *
     * @param predicate Predicate to be tested evaluated
     */

    S against(Predicate<A> predicate);

}
