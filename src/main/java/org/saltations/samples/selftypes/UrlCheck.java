package org.saltations.samples.selftypes;

import java.net.URL;

/**
 * Created by jmochel on 5/3/16.
 */
public class UrlCheck extends AbstractCheck<UrlCheck,URL>
{
    /**
     * Primary Constructor
     *
     * @param actual the url to test
     */

    public UrlCheck(URL actual)
    {
        super(UrlCheck.class, actual);
    }

}

