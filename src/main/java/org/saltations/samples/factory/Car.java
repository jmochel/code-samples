package org.saltations.samples.factory;

import java.awt.Color;

public abstract class Car
{
    private final Color color;

    @FunctionalInterface
    public interface Factory {
        Car make(Color color);
    }

    protected Car(Color color) {
        this.color = color;
    }

}