package org.saltations.samples.factory;

import java.awt.*;

public final class Nissan extends Car {
    public Nissan(Color color) {
        super(color);
    }
}