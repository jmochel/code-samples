package org.saltations.samples.factory;

import java.awt.Color;

public final class Volvo extends Car {
    public Volvo(Color color) {
        super(color);
    }
}