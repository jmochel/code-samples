package org.saltations.samples.factory;

import java.awt.*;

public class CarTrader
{
    private Car.Factory factory;

    public void setSupplier(Car.Factory factory)
    {
        this.factory = factory;
    }

    public Car buyCar(Color color)
    {
        return factory.make(color);
    }

}
