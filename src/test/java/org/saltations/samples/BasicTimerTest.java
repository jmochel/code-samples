package org.saltations.samples;


import java.util.Timer;
import java.util.TimerTask;

public class BasicTimerTest {

    private static class EggTimer
    {

        private final Timer timer = new Timer();

        private final int seconds;

        public EggTimer(int seconds)
        {
            this.seconds = seconds;
        }

        public void start()
        {
            timer.schedule(new TimerTask()
            {
                public void run() {
                    System.out.println("Your egg is ready!");
                    timer.cancel();
                }

            }, seconds * 1000);
        }

        public static void main(String[] args)
        {
            EggTimer eggTimer = new EggTimer(2);
            eggTimer.start();
        }
    }
}
