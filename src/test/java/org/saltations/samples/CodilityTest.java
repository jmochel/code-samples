package org.saltations.samples;


import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class CodilityTest {


    @Test
    public void testCorrectInput1()
    {
        Assertions.assertEquals(2, solution("(())"));
    }


    @Test
    public void testCorrectInput2()
    {
        Assertions.assertEquals(4, solution("(())))("));
    }


    @Test
    public void testCorrectInput3()
    {
        Assertions.assertEquals(2, solution("))"));
    }

    @Test
    public void testCorrectInput4()
    {
        Assertions.assertEquals(0, solution(""));
    }

    /*
     * Complete the function below.
     */

    int solution(String S)
    {
        if (S.isEmpty() )
        {
            return 0;
        }

        char[] array = S.toCharArray();

        int leftNdx = -1;
        int rightNdx = S.length();

        final int BALANCED = 0;
        final int NEEDS_CLOSE = 2;
        final int DONE = 3;

        int state = BALANCED;

        while (state != DONE && leftNdx < rightNdx)
        {
            switch (state)
            {
                case BALANCED:

                    leftNdx++;
                    if (leftNdx == rightNdx)
                    {
                        state = DONE;
                    }
                    else if (array[leftNdx] == '(')
                    {
                        state = NEEDS_CLOSE;
                    }

                    break;

                case NEEDS_CLOSE:

                    rightNdx--;

                    if (leftNdx == rightNdx)
                    {
                        state = DONE;
                    }
                    else if (array[rightNdx] == ')')
                    {
                        state = BALANCED;
                    }

                    break;
            }
        }


        return leftNdx;
    }






}