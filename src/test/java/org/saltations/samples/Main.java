package org.saltations.samples;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Main {

    public static void main(String[] args)
    {
        System.out.println("Hello, world!");

        test1();
        test2();
    }

    public static void test1()
    {
        List<Integer> list1 = Arrays.asList(1, 2, 2);
        List<Integer> list2 = Arrays.asList(2, 1, 2);

        if (isPermutation(list1, list2))
        {
            System.out.println("Success Test 1");
        }
    }

    public static void test2()
    {
        List<Integer> list1 = Arrays.asList(1, 1, 2);
        List<Integer> list2 = Arrays.asList(2, 1, 2);

        if (!isPermutation(list1, list2))
        {
            System.out.println("Success Test 2");
        }
    }

    public static <T> boolean isPermutation(List<T> l1, List<T> l2)
    {
        //
        // If unequal size, one cannot be a permutation of the other
        //

        if(l1.size() != l2.size())
        {
            return false;
        }

        //
        // Create a mapping of each value and the number of occurrences for each list.
        //

        Map<T, Integer> map1 = new HashMap<>();

        for (T elt : l1)
        {
            int  count = 1;

            if (map1.containsKey(elt))
            {
                count = map1.get(elt).intValue() + 1;
            }

            map1.put(elt, count);
        }

        Map<T, Integer> map2 = new HashMap<>();

        for (T elt : l2)
        {
            int count = 1;

            if (map2.containsKey(elt))
            {
                count = map2.get(elt).intValue() + 1;
            }

            map2.put(elt, count);
        }

        //
        // If they don't have the same keys, one is not a permutation of the other.
        //

        for (T key : map1.keySet())
        {
            if ( !map2.containsKey(key) || !map1.get(key).equals(map2.get(key)) )
            {
                return false;
            }
        }


        return true;
    }
}
