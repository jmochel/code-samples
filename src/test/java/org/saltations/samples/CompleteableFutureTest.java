package org.saltations.samples;

import com.jcabi.ssh.SSH;
import com.jcabi.ssh.Shell;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.System.out;

@Slf4j
public class CompleteableFutureTest
{
    private ExecutorService es = Executors.newFixedThreadPool(6);

    private Shell shell;

    @BeforeClass
    protected void setup() throws IOException
    {
        Config cfg = ConfigFactory.load();

        String address = cfg.getString("ssh-server.address");
        String login = cfg.getString("ssh-server.user-name");
        String keyFilePath = cfg.getString("ssh-server.key-file");

        shell = new SSH(address, login, new String(Files.readAllBytes(Paths.get(keyFilePath))));
    }

    @Test
    public void check() throws IOException
    {
        String cmd = "date && sleep 15s && echo $RANDOM && date ";

        List<CompletableFuture<String>> cfs = IntStream.range(1, 6).mapToObj(i -> exec(cmd)).collect(Collectors.toList());

        CompletableFuture<List<String>> cf = compose(cfs);

        try
        {
            List<String> strings = cf.get();

            strings.forEach(x -> out.println(x));
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        catch (ExecutionException e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Creates a CompletableFuture for an remotely executed command
     *
     * @param cmd
     * @return
     */

    private CompletableFuture<String> exec(String cmd)
    {
        CompletableFuture<String> cf = CompletableFuture.supplyAsync(() -> {

            String stdout = "";

            try
            {
                stdout = new Shell.Plain(shell).exec(cmd);
            }
            catch (IOException ex)
            {
            }

            return stdout;

        }, es);

        return cf;
    }

    /**
     * Compose all the futures together
     *
     * @param futures
     * @return
     */

    private CompletableFuture<List<String>> compose(List<CompletableFuture<String>> futures)
    {

        CompletableFuture<Void> allDoneFuture =  CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));

        return allDoneFuture.thenApply(x ->
                futures.stream().
                        map(future -> future.join()).
                        collect(Collectors.<String>toList())
        );
    }
}
