package org.saltations.samples;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MonitorExecutorsTest
{
    Logger log = LoggerFactory.getLogger(MonitorExecutorsTest.class);

    @Test
    @DisplayName("Exec callable (future)")
    public void check1() throws IOException, ExecutionException, InterruptedException {
        ExecutorService es = Executors.newFixedThreadPool(8);


        final List<Task> tasks = Arrays.asList(
                new Task("task1"),
                new Task("task2"),
                new Task("task3"),
                new Task("task4"),
                new Task("task5"),
                new Task("task6"),
                new Task("task7"),
                new Task("task8"),
                new Task("task9"),
                new Task("task10")
        );

        Runnable monitor = () -> {

            boolean done = false;

            try {
                while (!Done(tasks)) {
                    TimeUnit.MILLISECONDS.sleep(500);
                    tasks.forEach(x -> log.info("{} => {}", x.id , x.percent));
                }
            }
            catch (InterruptedException e)
            {
                throw new IllegalStateException("Tasks interrupted", e);
            }
        };

        tasks.forEach(t -> es.submit(t));
        es.submit(monitor);

        TimeUnit.SECONDS.sleep(20);

        shutdown(es);
    }

    boolean Done(List<Task> tasks)
    {
        return tasks.stream().allMatch(x -> x.percent == 100);
    }

    static class Task implements Runnable {

        int percent = 0;
        String id = "";

        public Task(String id) {
            this.percent = 0;
            this.id = id;
        }

        public void run()
        {
            while (percent < 100) {
                try {
                    Thread.sleep(1000);
                    makeProgress();
                } catch (InterruptedException e) {
                    throw new IllegalStateException("Tasks interrupted", e);
                }
             }
        }

        synchronized void makeProgress()
        {
            percent = percent + 10;
        }

        int getPercent()
        {
            return percent;
        }

    }

    void shutdown(ExecutorService es)
    {
        try {
            log.info("Attempting executor shutdown");
            es.shutdown();
            es.awaitTermination(12, TimeUnit.SECONDS);
        }
        catch (InterruptedException e)
        {
            log.error("Tasks interrupted", e);
        }
        finally {
            if (!es.isTerminated())
            {
                log.error("Cancelling non-finished tasks.");
            }
            es.shutdownNow();
            log.info("Shutdown finished");
        }
    }

}
