package org.saltations.samples;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
public class BasicCallableTest
{

    @Test
    public void test() {

        log.info("Start Test");

        Callable<Integer> callable = () -> {
            log.info("Call");
            return 8;
        };

        ExecutorService service =  Executors.newSingleThreadExecutor();
        Future<Integer> future = service.submit(callable);

        Integer result=0;

        try {
            result = future.get();

            assertEquals(8, result.intValue());

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }


}
