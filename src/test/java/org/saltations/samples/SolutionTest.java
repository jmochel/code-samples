package org.saltations.samples;


import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class SolutionTest {

    @Test
    public void testCorrectInput0()
    {
        System.out.println("T0");
        Assert.assertEquals(2,programmerStrings("progxrammerrxprogxgrammer") );
    }

    @Test
    public void testCorrectInput1()
    {
        Assert.assertEquals(2,programmerStrings("xprogxrmaxemrppprmmograeiruu") );
    }


    @Test
    public void testCorrectInput2()
    {
        Assert.assertEquals(0,programmerStrings("programmerprogrammer") );
    }

    @Test
    public void testInput3()
    {
        System.out.println("3");
        Assert.assertEquals(0,programmerStrings("programmer") );
    }

    @Test
    public void testInput4()
    {
        System.out.println("4");
        Assert.assertEquals(2,programmerStrings("programmerprogrammerxprogrammer") );
    }

    @Test
    public void testInput5()
    {
        Assert.assertEquals(0,programmerStrings("xprogrammer") );
    }

    @Test
    public void testInput6()
    {
        Assert.assertEquals(0,programmerStrings("rammer") );
    }


    /*
     * Complete the function below.
     */

    static int programmerStrings(String s)
    {
        /*
         * Indicate that the current character must be skipped to create a break between the 'words'.
         */

        boolean skip = false;

        /*
         * Tracks the current counts of each character
         */

        Map<Character,Integer> countsByCharacter = new HashMap<>();
        List<Integer> sortedEndPoints = new ArrayList<>();
        List<Integer> startPoints = new ArrayList<>();

        /*
         * Walk through the characters, accumulating counts for each character until we have enough characters to
         * form a "programmmer string". When we do, count it, clear out the accumulation, create a break between
         * that word and the potential next word and then, start accumulating again.
         */

        for ( int idx = 0; idx < s.length(); idx++ ) {
            /*
             * Accumulate the count for the characters
             */

            char c = s.charAt(idx);

            int currCount = 1;

            if (countsByCharacter.containsKey(c)) {
                currCount = countsByCharacter.get(c).intValue() + 1;
            }

            countsByCharacter.put(c, currCount);

            /*
             * If the collected characters and their counts are at sufficient to spell
             * programmer increase the number of programmer strings found and clear the map out
             */

            if (countsByCharacter.containsKey(Character.valueOf('p')) &&
                    countsByCharacter.containsKey(Character.valueOf('r')) &&
                    countsByCharacter.containsKey(Character.valueOf('o')) &&
                    countsByCharacter.containsKey(Character.valueOf('g')) &&
                    countsByCharacter.containsKey(Character.valueOf('a')) &&
                    countsByCharacter.containsKey(Character.valueOf('m')) &&
                    countsByCharacter.containsKey(Character.valueOf('e')) &&
                    countsByCharacter.get(Character.valueOf('r')) >= 3 &&
                    countsByCharacter.get(Character.valueOf('m')) >= 2) {

                countsByCharacter.clear();
                sortedEndPoints.add(idx);
            }
        }

        /*
         * Walk through the characters, accumulating counts for each character until we have enough characters to
         * form a "programmmer string". When we do, count it, clear out the accumulation, create a break between
         * that word and the potential next word and then, start accumulating again.
         */

        for ( int idx = s.length() -1; idx >= 0; idx-- ) {
            /*
             * Accumulate the count for the characters
             */

            char c = s.charAt(idx);

            int currCount = 1;

            if (countsByCharacter.containsKey(c)) {
                currCount = countsByCharacter.get(c).intValue() + 1;
            }

            countsByCharacter.put(c, currCount);

            /*
             * If the collected characters and their counts are at sufficient to spell
             * programmer increase the number of programmer strings found and clear the map out
             */

            if (countsByCharacter.containsKey(Character.valueOf('p')) &&
                    countsByCharacter.containsKey(Character.valueOf('r')) &&
                    countsByCharacter.containsKey(Character.valueOf('o')) &&
                    countsByCharacter.containsKey(Character.valueOf('g')) &&
                    countsByCharacter.containsKey(Character.valueOf('a')) &&
                    countsByCharacter.containsKey(Character.valueOf('m')) &&
                    countsByCharacter.containsKey(Character.valueOf('e')) &&
                    countsByCharacter.get(Character.valueOf('r')) >= 3 &&
                    countsByCharacter.get(Character.valueOf('m')) >= 2) {

                countsByCharacter.clear();
                startPoints.add(idx);
            }
        }

        List<Integer> sortedStartPoints =  startPoints.stream().sorted().collect(java.util.stream.Collectors.toList());

        for (int i = 0; i < sortedStartPoints.size(); i++) {

            int start = sortedStartPoints.get(i);
            int end = sortedEndPoints.get(i);

            System.out.printf("%s,%s%n", start, end);
        }

        /*
         * Count the indixes
         */

        int indices = 0;

        for (int i = 0; i < sortedStartPoints.size(); i++)
        {
            int start = sortedStartPoints.get(i);
            int end = sortedEndPoints.get(i);

            if (i < sortedStartPoints.size()-1)
            {
                indices = indices + (sortedStartPoints.get(i+1) - (end+1));
            }
        }

        return indices;
    }


    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        final String fileName = System.getenv("OUTPUT_PATH");
        BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        int res;
        String _s;
        try {
        _s = in.nextLine();
        } catch (Exception e) {
        _s = null;
        }

        res = programmerStrings(_s);
        bw.write(String.valueOf(res));
        bw.newLine();

        bw.close();
   }
}