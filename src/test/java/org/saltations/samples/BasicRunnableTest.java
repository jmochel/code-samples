package org.saltations.samples;

import com.jcabi.ssh.Shell;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;


@Slf4j
public class BasicRunnableTest
{
    private Shell shell;

    @Test
    public void test() {

        log.info("Start Test");

        Runnable runnable = () -> {
            log.info("Run");
        };

        Thread thread = new Thread(runnable);
        thread.start();
    }


}
