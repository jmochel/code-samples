package org.saltations.samples;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

import static java.lang.System.out;
import static org.junit.jupiter.api.Assertions.expectThrows;

public class ExecutorsTest
{
    Logger log = LoggerFactory.getLogger(ExecutorsTest.class);

    @Test
    @DisplayName("Exec runnable")
    public void check0() throws IOException
    {
        ExecutorService es = Executors.newSingleThreadExecutor();

        es.submit(()-> log.info("Hello {}", Thread.currentThread().getName()));

        shutdown(es);
    }

    @Test
    @DisplayName("Exec callable (future)")
    public void check1() throws IOException, ExecutionException, InterruptedException {
        ExecutorService es = Executors.newFixedThreadPool(1);

        Callable<Integer> task = () -> {

            try
            {
                TimeUnit.SECONDS.sleep(1);
                return 123;
            }
            catch (InterruptedException e)
            {
                throw new IllegalStateException("Tasks interrupted", e);
            }
        };

        Future<Integer> future = es.submit(task);

        Integer result = future.get();

        es.submit(()-> log.info("Future is done ? {}, result {}", future.isDone(), result));

        shutdown(es);
    }

    @Test
    @DisplayName("Exec callable (future) with timeout")
    public void check2() throws IOException, ExecutionException, InterruptedException {

        ExecutorService es = Executors.newFixedThreadPool(1);

        Future<Integer> future = es.submit(() -> {

            try
            {
                TimeUnit.SECONDS.sleep(3);
                return 123;
            }
            catch (InterruptedException e)
            {
                throw new IllegalStateException("Tasks interrupted", e);
            }
        });

        Throwable exception = expectThrows(TimeoutException.class, () -> {
            future.get(1,TimeUnit.SECONDS);
        });

        shutdown(es);
    }

    @Test
    @DisplayName("Exec multiple callables (future)")
    public void check3() throws IOException, ExecutionException, InterruptedException {

        ExecutorService es = Executors.newWorkStealingPool();

        List<Callable<String>> callables = Arrays.asList(
                () -> "task1",
                () -> "task2",
                () -> "task3"
        );

        Collection<Future<String>> futures = es.invokeAll(callables);

        futures.stream()
                .map(future -> {
                    try
                    {
                        return future.get();
                    }
                    catch (Exception e)
                    {
                        throw new IllegalStateException(e);
                    }
                })
                .forEach(out::println);

        shutdown(es);
    }

    @Test
    @DisplayName("Exec runnable (schedulable) ")
    public void check4() throws IOException, ExecutionException, InterruptedException {

        ScheduledExecutorService es = Executors.newScheduledThreadPool(1);

        Runnable task = () -> {
            out.println("Scheduling " + System.nanoTime());
        };

        ScheduledFuture<?> future = es.schedule(task, 3, TimeUnit.SECONDS);

        TimeUnit.MILLISECONDS.sleep(1337);

        log.info("Remaining delay : {}",future.getDelay(TimeUnit.MILLISECONDS));

        shutdown(es);
    }

    @Test
    @DisplayName("Exec runnable (schedulable) as heartbeat")
    public void check5() throws IOException, ExecutionException, InterruptedException {

        ScheduledExecutorService es = Executors.newScheduledThreadPool(1);

        Runnable task = () -> {
            out.println("Scheduling " + System.nanoTime());
        };

        ScheduledFuture<?> future = es.scheduleAtFixedRate(task, 0, 1, TimeUnit.SECONDS);

        TimeUnit.SECONDS.sleep(3);

        shutdown(es);
    }


    @Test
    @DisplayName("Examine ForkJoinPools")
    public void check6() throws IOException, ExecutionException, InterruptedException {

        ForkJoinPool pool = new ForkJoinPool();

        int threads = pool.getParallelism();
        out.println("Threads " + threads);

    }

    void shutdown(ExecutorService es)
    {
        try {
            log.info("Attempting executor shutdown");
            es.shutdown();
            es.awaitTermination(5, TimeUnit.SECONDS);
        }
        catch (InterruptedException e)
        {
            log.error("Tasks interrupted", e);
        }
        finally {
            if (!es.isTerminated())
            {
                log.error("Cancelling non-finished tasks.");
            }
            es.shutdownNow();
            log.info("Shutdown finished");
        }
    }

}
