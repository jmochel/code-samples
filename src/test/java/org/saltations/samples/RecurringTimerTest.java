package org.saltations.samples;


import org.junit.jupiter.api.Test;

import java.util.Timer;
import java.util.TimerTask;

public class RecurringTimerTest {

    private final Timer timer = new Timer();

    @Test
    public void test() throws InterruptedException
    {
        EggTimer eggTimer1 = new EggTimer(timer, 2, "Test 1");
        EggTimer eggTimer2 = new EggTimer(timer, 2, "Test 2");
        EggTimer eggTimer3 = new EggTimer(timer, 2, "Test 3");

        eggTimer1.start();
        eggTimer2.start();
        eggTimer3.start();

        Thread.sleep(3000);

        timer.cancel();
    }

    private static class EggTimer
    {
        private final int seconds;
        private final Timer timer;
        private final String text;

        public EggTimer(Timer timer, int seconds, String text)
        {
            this.timer = timer;
            this.seconds = seconds;
            this.text = text;
        }

        public void start()
        {
            timer.schedule(new TimerTask()
            {
                public void run()
                {
                    System.out.println(text);
                }

            }, seconds * 1000);
        }

    }

}
