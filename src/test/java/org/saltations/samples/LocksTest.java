package org.saltations.samples;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.StampedLock;
import java.util.stream.IntStream;

import static java.lang.System.out;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class LocksTest
{
    private static final Logger log = LoggerFactory.getLogger(LocksTest.class);

    public static class Incrementable
    {
        int count = 0;

        void increment()
        {
            count = count + 1;
        }
    }

    public static class LockedIncrementable
    {
        int count = 0;

        synchronized void increment()
        {
            count = count + 1;
        }
    }

    public static class ReentrantLockedIncrementable
    {
        ReentrantLock lock = new ReentrantLock();
        int count = 0;

        synchronized void increment()
        {
            lock.lock();
            try
            {
                count++;
            }
            finally {
                lock.unlock();
            }
        }
    }


    @Test
    @DisplayName("Exec unsynchronized")
    public void check0() throws IOException
    {
        ExecutorService es = Executors.newFixedThreadPool(2);

        final Incrementable incrementable = new Incrementable();

        IntStream.range(0,10000)
                .forEach(i -> es.submit(() -> incrementable.increment()));

        shutdown(es);

        assertNotEquals(10000, incrementable.count);
    }


    @Test
    @DisplayName("Exec synchronized")
    public void check1() throws IOException
    {
        final LockedIncrementable incrementable = new LockedIncrementable();

        ExecutorService es = Executors.newFixedThreadPool(2);
        IntStream.range(0,10000)
                .forEach(i -> es.submit(() -> incrementable.increment()));
        shutdown(es);

        assertEquals(10000, incrementable.count);
    }

    @Test
    @DisplayName("Exec reentrantly locked ")
    public void check3() throws IOException
    {
        final ReentrantLockedIncrementable incrementable = new ReentrantLockedIncrementable();

        ExecutorService es = Executors.newFixedThreadPool(2);
        IntStream.range(0,10000)
                .forEach(i -> es.submit(() -> incrementable.increment()));
        shutdown(es);

        assertEquals(10000, incrementable.count);
    }


    @Test
    @DisplayName("Exec finer grain reentrant lock")
    public void check4() throws IOException
    {
        ExecutorService es = Executors.newFixedThreadPool(2);

        ReentrantLock lock = new ReentrantLock();

        es.submit(() -> {
            lock.lock();

            try
            {
                sleep(1);
            }
            finally {
                lock.unlock();
            }
        });

        es.submit(() -> {
            out.println("Locked ? " + lock.isLocked());
            out.println("Held by me ? " + lock.isHeldByCurrentThread());

            boolean locked = lock.tryLock();
            out.println("Lock acquired ? " + locked);
        });

        shutdown(es);
    }

    @Test
    @DisplayName("Exec read/write locked ")
    public void check5() throws IOException
    {
        ExecutorService es = Executors.newFixedThreadPool(2);
        Map<String,String> map = new HashMap<>();

        ReadWriteLock lock = new ReentrantReadWriteLock();

        es.submit(() -> {
            lock.writeLock().lock();

            try
            {
                sleep(1);
                map.put("foo","bar");
            }
            finally {
                lock.writeLock().unlock();
            }
        });

        Runnable readTask = () -> {
            lock.readLock().lock();

            try
            {
                out.println(map.get("foo"));
                sleep(1);
            }
            finally {
                lock.readLock().unlock();
            }
        };

        es.submit(readTask);
        es.submit(readTask);

        shutdown(es);
    }



    @Test
    @DisplayName("Exec read/write stamped lock")
    public void check6() throws IOException
    {
        ExecutorService es = Executors.newFixedThreadPool(2);
        Map<String,String> map = new HashMap<>();

        StampedLock lock = new StampedLock();

        es.submit(() -> {
            long stamp = lock.writeLock();

            try
            {
                sleep(1);
                map.put("foo","bar");
            }
            finally {
                lock.unlock(stamp);
            }
        });

        Runnable readTask = () -> {
            long stamp = lock.readLock();

            try
            {
                out.println(map.get("foo"));
                sleep(1);
            }
            finally {
                lock.unlockRead(stamp);
            }
        };

        es.submit(readTask);
        es.submit(readTask);

        shutdown(es);
    }

    void shutdown(ExecutorService es)
    {
        try {
            log.info("Attempting executor shutdown");
            es.shutdown();
            es.awaitTermination(5, TimeUnit.SECONDS);
        }
        catch (InterruptedException e)
        {
            log.error("Tasks interrupted", e);
        }
        finally {
            if (!es.isTerminated())
            {
                log.error("Cancelling non-finished tasks.");
            }
            es.shutdownNow();
            log.info("Shutdown finished");
        }
    }

    static void sleep(long seconds)
    {
        try
        {
            TimeUnit.SECONDS.sleep(seconds);
        }
        catch (InterruptedException e)
        {
            throw new IllegalStateException(e);
        }
    }



}
