package org.saltations.samples;

import org.junit.jupiter.api.Test;
import org.saltations.samples.selftypes.UrlCheck;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class UrlCheckTest
{
    @Test
    public void checkUrl() throws MalformedURLException
    {
        URL url = new URL("http://google.com");

        UrlCheck check = new UrlCheck(url);
        check.as("Basic URL").against(a -> a.getProtocol().equals("http"));
    }

    @Test
    public void breakUrl() throws MalformedURLException
    {
        URL url = new URL("http://google.com");


        UrlCheck check = new UrlCheck(url);

        assertThrows(IllegalArgumentException.class, ()->check.as("Basic URL").against(a -> a.getProtocol().equals("https")));

    }
}
